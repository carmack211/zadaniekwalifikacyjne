package poloczek.grzegorz.pl.zadaniekwalifikacyjne;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.adapter.FirstRowAdapter;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.Footer;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.Item;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils.AsyncTaskForDialog;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils.ListCreator;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.name_container)
    TextView nameContainer;

    private List<Item> rowItemList;

    private AsyncTask asyncTask;

    private RecyclerView recyclerView;

    private boolean doubleBackToExitPressedOnce = false;

    private boolean hasMore;

    ItemTouchHelper.SimpleCallback simpleCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                    startActivity(intent);
                    finish();
                }
            };

    private RecyclerView.OnScrollListener getRecyclerEndScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (hasMore && !(hasFooter()) && rowItemList.size()<200) {
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                if (layoutManager.findLastCompletelyVisibleItemPosition()
                        == layoutManager.getItemCount() - 2) {
                    rowItemList.add(new Footer());
                    Handler handler = new Handler();

                    final Runnable runnable = new Runnable() {
                        public void run() {
                            MainActivity.this.recyclerView.getAdapter().notifyItemInserted(rowItemList.size() - 1);
                        }
                    };
                    handler.post(runnable);
                    asyncTask = new RecyclerLoader();
                    asyncTask.execute((Object[]) null);
                }
            }
        }
    };

    private boolean hasFooter() {
        return rowItemList.get(rowItemList.size() - 1) instanceof Footer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        hasMore = true;
        nameContainer.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    nameContainer.setTextColor(getResources().getColor(R.color.red));
                } else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    nameContainer.setTextColor(getResources().getColor(R.color.green));
                }
                return false;
            }
        });
        initRecycler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initRecycler();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (asyncTask != null) {
            asyncTask.cancel(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.back_pressed_toast_text, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    public void runDialog(View view) {
        new AsyncTaskForDialog(nameContainer, this, true).execute(100);
    }

    public void initRecycler() {
        rowItemList = ListCreator.createList(100, true);
        recyclerView = findViewById(R.id.item_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setAdapter(new FirstRowAdapter(rowItemList));
        recyclerView.setLayoutManager(linearLayoutManager);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.addOnScrollListener(getRecyclerEndScrollListener);
    }

    private class RecyclerLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Log.e(this.getClass().toString(), e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            int size = rowItemList.size();
            rowItemList.remove(size - 1);
            rowItemList.addAll(ListCreator.generateNewData(size));
            recyclerView.getAdapter().notifyItemRangeChanged(size - 1, rowItemList.size() - size);
        }
    }

}


