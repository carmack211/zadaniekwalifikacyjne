package poloczek.grzegorz.pl.zadaniekwalifikacyjne.adapter;


import android.view.View;

public interface AdapterCallback {
    View.OnClickListener onViewClick(View view, View parent);
}
