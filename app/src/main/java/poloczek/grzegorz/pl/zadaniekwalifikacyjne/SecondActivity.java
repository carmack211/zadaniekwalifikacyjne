package poloczek.grzegorz.pl.zadaniekwalifikacyjne;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;
import java.util.List;
import butterknife.ButterKnife;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.adapter.AdapterCallback;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.adapter.SecondRowAdapter;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonTag;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.Item;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils.DialogHelper;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils.ListCreator;

public class SecondActivity extends AppCompatActivity implements AdapterCallback {

    private SecondRowAdapter secondListRowAdapter;

    private List<Item> rowItemList;

    private boolean doubleBackToExitPressedOnce = false;

    ItemTouchHelper.SimpleCallback simpleCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);
        initRecycler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initRecycler();
    }

    public void initRecycler(){
        rowItemList = ListCreator.createList(20,false);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.second_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        secondListRowAdapter = new SecondRowAdapter(this, this, rowItemList);

        recyclerView.setAdapter(secondListRowAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

    @Override
    public View.OnClickListener onViewClick(View view, final View viewParent) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() instanceof ButtonTag) {
                    switch ((ButtonTag) view.getTag()) {
                        case BUTTON_ONE:
                            DialogHelper.showDialog(view.getContext(), getResources().getString(R.string.dialog_message));
                            break;
                        case BUTTON_TWO:
                            view.setVisibility(View.GONE);
                            break;
                        case BUTTON_THREE:
                            viewParent.findViewById(R.id.first_line_button_one).setVisibility(View.INVISIBLE);
                            break;
                    }
                }
            }
        };
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.back_pressed_toast_text, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
