package poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils;

import java.util.ArrayList;
import java.util.List;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.R;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonOne;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonThree;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonTwo;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.Item;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.RowItem;

public class ListCreator {

    public static List<Item> createList(int listItems, boolean defaultType) {
        List<Item> list = new ArrayList<>();
        int i = 1;
        int icon;
        if(!defaultType) {
            list.add(new ButtonOne());
            list.add(new ButtonTwo());
            list.add(new ButtonThree());
            i=4;
        }
        for (;i <= listItems; i++) {
            icon = generateIcon(i);
            RowItem item = new RowItem("Item: " + i, icon);
            list.add(item);
        }
        return list;
    }

    public static ArrayList<Item> generateNewData(int size) {
        ArrayList<Item> newData = new ArrayList<>(100);
        int icon;
        for (int i = 0; i < 20; i++) {
            icon = generateIcon(i);
            RowItem item = new RowItem("Item: " + (size+i), icon);
            newData.add(item);
        }

        return newData;
    }

    private static int generateIcon(int i){
        int generatedIcon = R.mipmap.ic_launcher;
        switch(i%3){
            case 0:
                generatedIcon = R.mipmap.ic_launcher;
                break;
            case 1:
                generatedIcon = R.drawable.android;
                break;
            case 2:
                generatedIcon = R.drawable.instagram;
                break;
        }
        return generatedIcon;
    }

}
