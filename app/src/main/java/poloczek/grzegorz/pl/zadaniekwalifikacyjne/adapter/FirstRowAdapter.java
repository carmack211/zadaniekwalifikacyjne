package poloczek.grzegorz.pl.zadaniekwalifikacyjne.adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.List;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.R;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.Footer;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.Item;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.RowItem;


public class FirstRowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ROW_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private List<Item> rowItemList;

    public FirstRowAdapter(@NonNull List<Item> rowItemList) {
        this.rowItemList = rowItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ROW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_list_first, parent, false);
            return new FirstListRowHolder(view);
        }else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_progress_bar,
                    parent, false);
            return new FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FirstListRowHolder) {
            RowItem rowItem = (RowItem) rowItemList.get(position);
            ((FirstListRowHolder) holder).itemTextView.setText(rowItem.getName());
            ((FirstListRowHolder) holder).itemImageView.setImageResource(rowItem.getIcon());
        }
    }


    @Override
    public int getItemCount() {
        return rowItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (rowItemList.get(position) instanceof RowItem) {
            return TYPE_ROW_ITEM;
        } else if (rowItemList.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    public class FirstListRowHolder extends RecyclerView.ViewHolder {
        private TextView itemTextView;
        private ImageView itemImageView;
        public FirstListRowHolder(View itemView) {
            super(itemView);
            itemTextView = (TextView) itemView.findViewById(R.id.item_text);
            itemImageView = (ImageView) itemView.findViewById(R.id.image_left);
        }
    }

    private static class FooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar getProgressBar() {
            return progressBar;
        }

        private ProgressBar progressBar;

        public FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.main_progress_bar);
        }
    }
}
