package poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class DialogHelper {

    private static ProgressDialog progressDialog;

    public static AlertDialog showDialog(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(true)
                .setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    public static ProgressDialog showProgressDialog(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setProgress(20);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        return progressDialog;
    }

    public static void centerCancelButtonOnProgressDialog(){
        final Button cancelButton = progressDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams cancelButtonParams = (LinearLayout.LayoutParams) cancelButton.getLayoutParams();
        cancelButtonParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        cancelButton.setLayoutParams(cancelButtonParams);
    }
}

