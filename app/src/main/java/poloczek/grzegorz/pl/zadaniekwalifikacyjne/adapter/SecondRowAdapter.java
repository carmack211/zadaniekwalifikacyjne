package poloczek.grzegorz.pl.zadaniekwalifikacyjne.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import poloczek.grzegorz.pl.zadaniekwalifikacyjne.R;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonOne;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonTag;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonThree;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.ButtonTwo;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.Item;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.model.RowItem;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils.AsyncTaskForDialog;


public class SecondRowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_BUTTON_ONE = 1;
    private static final int TYPE_BUTTON_TWO = 2;
    private static final int TYPE_BUTTON_THREE = 3;
    private static final int DEFAULT = 4;

    private List<Item> rowItemList;
    private Context context;
    private AdapterCallback callback;

    public SecondRowAdapter(Context context, AdapterCallback adapterCallback, @NonNull List<Item> rowItemList) {
        this.context = context;
        this.callback = adapterCallback;
        this.rowItemList = rowItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_BUTTON_ONE) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_line_first,
                    parent, false);
            return new SecondRowAdapter.FirstRowHolder(row, callback);
        } else if (viewType == TYPE_BUTTON_TWO) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_line_second,
                    parent, false);
            return new SecondRowAdapter.SecondRowHolder(row);
        } else if (viewType == TYPE_BUTTON_THREE) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_line_third,
                    parent, false);
            return new SecondRowAdapter.ThirdRowHolder(row);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_list_second, parent, false);
            return new SecondRowAdapter.SecondListRowHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SecondRowAdapter.SecondListRowHolder) {
            RowItem rowItem = (RowItem) rowItemList.get(position);
            ((SecondListRowHolder) holder).itemTextView.setText(rowItem.getName());
            ((SecondListRowHolder) holder).itemImageView.setImageResource(rowItem.getIcon());
        }
    }

    @Override
    public int getItemCount() {
        return rowItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (rowItemList.get(position) instanceof ButtonOne) {
            return TYPE_BUTTON_ONE;
        } else if (rowItemList.get(position) instanceof ButtonTwo) {
            return TYPE_BUTTON_TWO;
        } else if (rowItemList.get(position) instanceof ButtonThree) {
            return TYPE_BUTTON_THREE;
        } else if (rowItemList.get(position) instanceof Item) {
            return DEFAULT;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    public class SecondListRowHolder extends RecyclerView.ViewHolder {
        private TextView itemTextView;
        private ImageView itemImageView;

        public SecondListRowHolder(View itemView) {
            super(itemView);
            itemTextView = itemView.findViewById(R.id.second_item_text);
            itemImageView = itemView.findViewById(R.id.image_right);
        }
    }

    public class FirstRowHolder extends RecyclerView.ViewHolder {
        private Button button1;
        private Button button2;
        private Button button3;

        FirstRowHolder(View itemView, AdapterCallback callback) {
            super(itemView);
            button1 = itemView.findViewById(R.id.first_line_button_one);
            button2 = itemView.findViewById(R.id.first_line_button_two);
            button3 = itemView.findViewById(R.id.first_line_button_three);
            button1.setTag(ButtonTag.BUTTON_ONE);
            button2.setTag(ButtonTag.BUTTON_TWO);
            button3.setTag(ButtonTag.BUTTON_THREE);
            button1.setOnClickListener(callback.onViewClick(button1, itemView));
            button2.setOnClickListener(callback.onViewClick(button2, itemView));
            button3.setOnClickListener(callback.onViewClick(button2, itemView));
        }
    }

    public class SecondRowHolder extends RecyclerView.ViewHolder {
        private Button hugeButton;

        SecondRowHolder(View itemView) {
            super(itemView);
            hugeButton = itemView.findViewById(R.id.second_line_button);
            hugeButton.setOnClickListener(onHugeButtonClick());

        }

        View.OnClickListener onHugeButtonClick() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncTaskForDialog(context, false).execute(100);
                }
            };
        }
    }

    public class ThirdRowHolder extends RecyclerView.ViewHolder {
        ThirdRowHolder(View itemView) {
            super(itemView);
        }
    }

}
