package poloczek.grzegorz.pl.zadaniekwalifikacyjne.utils;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import poloczek.grzegorz.pl.zadaniekwalifikacyjne.R;

public class AsyncTaskForDialog extends AsyncTask<Integer, Integer, Integer> {

    private ProgressDialog progressDialog;
    private Context context;
    private TextView textView;
    private boolean isCancel;

    public AsyncTaskForDialog(TextView textView, Context context, boolean cancelButtonAvailable) {
        this.textView = textView;
        this.context = context;
        initProgressDialog(context, cancelButtonAvailable);
    }

    public AsyncTaskForDialog(Context context, boolean cancelButtonAvailable) {
        this.context = context;
        initProgressDialog(context, cancelButtonAvailable);
    }

    private void initProgressDialog(Context context, boolean cancelButtonAvailable) {
        progressDialog = DialogHelper.showProgressDialog(context);
        if (cancelButtonAvailable) {
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isCancel = true;
                    onCancelled();
                    dialog.dismiss();
                }
            });
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progressDialog.show();

        DialogHelper.centerCancelButtonOnProgressDialog();

        if (textView != null) {
            textView.setTextColor(context.getResources().getColor(R.color.white));
            textView.setBackgroundColor(context.getResources().getColor(R.color.black));
        }
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        Integer value = integers[0];
        for (int i = 0; i <= value; i = i + 20) {
            try {
                Thread.sleep(1000);
                publishProgress((int) i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        Integer value = values[0];
        progressDialog.setProgress(values[0]);
        progressDialog.setMessage(value.toString());
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        if (!isCancel) {
            if (textView != null) {
                textView.setBackgroundColor(context.getResources().getColor(R.color.white));
                textView.setTextColor(context.getResources().getColor(R.color.blue));
                textView.setClickable(false);
                textView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        return true;
                    }
                });
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (textView != null) {
            textView.setBackgroundColor(context.getResources().getColor(R.color.white));
            textView.setTextColor(context.getResources().getColor(R.color.green));
        }
    }
}
